package dao;

import java.sql.Connection;
import java.sql.SQLException;

import dao.exception.InstallationException;
import dao.exception.PKException;
import dao.exception.ValidationException;

/**
 * Fabrique concrète pour FB
 * 
 * @author Didier
 *
 */
//@Slf4j
public class FBDAOFactory extends DAOFactory {

	private Connection connection;

	private ICableDao cableDAO = null;

	/**
	 * @param connection
	 */
	public FBDAOFactory(Connection connection) {
		this.connection = connection;
	}

//crée un cableDao si pas encore fait
	@Override
	public ICableDao getCableDAO() {
		if (cableDAO == null)
			cableDAO = new SQLCableDao(this);
		return cableDAO;
	}

	@Override
	public Connection getConnection() {
		return connection;
	}

	@Override
	protected void dispatchException(Exception e, String detail) throws InstallationException {
		if (e instanceof SQLException exc) {
			switch (exc.getErrorCode()) {
			case 335544665 -> throw new PKException("Problème d'identifiant", detail);
			case 335544347, 335544914 -> throw new ValidationException("Problème de validation: ", detail);
			default -> throw new InstallationException("Erreur...?");
			}
		}
		throw new InstallationException("Problème x ?");
	}

}
