package dao;

import java.util.List;

import model.Installation;
import model.RInstallation;

public interface IInstallationDao extends IDAO<Installation, Integer> {
	List<RInstallation> getListeRecord();
}
