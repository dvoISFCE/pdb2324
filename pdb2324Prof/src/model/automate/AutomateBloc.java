package model.automate;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import dao.DAOFactory;
import model.appareil.Appareil;
import model.appareil.Interrupteur;
import model.appareil.Prise;

public class AutomateBloc {
//expression régulière de validation de l'expression
	private static final String REG_EXPR = "(P|P[1..5])|(I|I[1-6])(P|P[1-5])";
			//Version avec les lampes: "(P|P[1..5])|(I|I[1-6])((P|P[1-5])|(L|L[1-9]))";

//permet de savoir rapidement si une expression est valide
	public static final boolean exprIsValid(String expr) {
		return expr.matches(REG_EXPR);
	}

//Clés associés aux possibilités d'états
	enum ETAT {
		INIT, P, GP, I, GI, L, GL, END
	};

	// Map ETAT --> objet Etat
	private Map<ETAT, Etat> etats = new HashMap<>();

	// Etat actuel
	private ETAT etatActuel = ETAT.INIT;

	// DAO
/*	private IPriseDao daoPrise;
	private IInterrupteurDao daoInterrupteur;
*/
	// Liste des appareils construite lors de l'évaluation de l'expression
	private List<Appareil> appareils;

	// Expression à déchiffrer
	private String expr;

	// Indice dans l'expression
	private int i;

	/**
	 * @param fabrique
	 * @param expr
	 */
	public AutomateBloc(DAOFactory fabrique) {
	/*	this.daoPrise = fabrique.getPriseDAO();
		this.daoInterrupteur = fabrique.getInterrupteurDAO();
	*/
		this.etatActuel = ETAT.INIT;
		// Crée tous les états
		etats.put(ETAT.INIT, new EtatInit(this));
		etats.put(ETAT.P, new EtatPrise(this));
		etats.put(ETAT.GP, new EtatGroupePrise(this));
		etats.put(ETAT.I, new EtatInterrupteur(this));
		etats.put(ETAT.GI, new EtatGroupeInterrupteur(this));
	}

	/**
	 * Evalue l'expression 
	 * @param expr expression
	 * @return liste d'appareils spécifié via l'expression
	 * @throws UnexpectedCharacterException
	 */
	public List<Appareil> generateListFromExpr(String expr) throws UnexpectedCharacterException {
		this.etatActuel = ETAT.INIT;
		this.expr = expr + '^';// ajoute le caractère de terminaison '^'
		// Création d'une liste vide d'appareil
		this.appareils = new ArrayList<>();
		// on se met avant le premier caractère car le 1ère n'est pas encore traité
		this.i = -1;
		Etat etat;
		while (etatActuel != ETAT.END && hasNext()) {
			etat = etats.get(etatActuel);
			etat.actionIn();
		}
		return appareils;
	}

	/*************** Visibilité package Package***********************/
	/* Avancement sur l'expression*/
	/**
	 * indique s'il reste des caractères à exploiter dans l'expression
	 * @return
	 */
	boolean hasNext() {
		return i < expr.length() - 1;
	}

	/**
	 * retourne la caractère courant
	 * @return
	 */
	char getCar() {
		return expr.charAt(i);
	}

	/**
	 * avance et retourne le caractère
	 * @return
	 */
	char next() {
		if (i < expr.length())
			i++;
		return expr.charAt(i);
	}

	/* ACTIONS IN utilisée par les états*/
	void actionCreePrise() {
		Prise p = //TOD0 A supprimer pour remplacer par la ligne suivante
				new Prise("PC11", "Prise Classique terre/Enf", 2,
				"m0 0 3 0c0 1 1 2 2 2m0-4c-1 0-2 1-2 2m0-2 0 4m2 1 0-1m0-4 0-1m-2 3", true, true, false);
				//daoPrise.getFromID("PC11").orElseThrow();
		appareils.add(p);
	}

	void actionCreeInterrupteur(String type) {
		Interrupteur i = //TOD0 A supprimer pour remplacer par la ligne suivante
				new Interrupteur("IC1", "Interrupteur Classique 1C", 1,
						"m0 0a1 1 0 001 1 1 1 0 001-1.006 1 1 0 00-1-.994 1 1 0 00-1 1m1.662-.74 2.338-2.26.482.519m-2.475 2.475",
						2, 1);
				//daoInterrupteur.getFromID(type).orElseThrow();
		appareils.add(i);
	}

	/**
	 * 
	 * @param nextState
	 */
	void nextState(ETAT nextState) {
		this.etatActuel = nextState;
	}
}
