package services;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.concurrent.Flow.Subscriber;

import dao.DAOFactory;
import dao.exception.InstallationException;
import dao.exception.LigneBlocSizeException;
import lombok.Getter;
import lombok.extern.slf4j.Slf4j;
import model.Adresse;
import model.Bloc;
import model.GroupeLigne;
import model.IInstallationItems;
import model.Installation;
import model.Installation.TypeLogement;
import model.Ligne;
import model.RInstallation;
import model.appareil.Appareil;
import model.appareil.CodeApp;
import model.appareil.Compteur;
import model.appareil.Disjoncteur;
import model.appareil.DisjoncteurDiff;
import model.appareil.Interrupteur;
import model.appareil.Prise;
import model.automate.AutomateBloc;
import model.automate.UnexpectedCharacterException;
import services.ListMessages.Classe;
import services.ListMessages.Evenement;

@Slf4j
public class Facade {
	// Fake data
	private static final Prise prise = new Prise("PC11", "Prise Classique terre/Enf", 2,
			"m0 0 3 0c0 1 1 2 2 2m0-4c-1 0-2 1-2 2m0-2 0 4m2 1 0-1m0-4 0-1m-2 3", true, true, false);

	private static final Interrupteur int1 = new Interrupteur("IC1", "Interrupteur Classique 1C", 1,
			"m0 0a1 1 0 001 1 1 1 0 001-1.006 1 1 0 00-1-.994 1 1 0 00-1 1m1.662-.74 2.338-2.26.482.519m-2.475 2.475",
			2, 1);

	private static final Disjoncteur fu22 = new Disjoncteur("FU22", "Fusible automatique 20A 2Phases", 2,
			"m 0 0 l 0 -1 l -1 -2 l -0.045 0.029 l 0.096 0.192 l 0.043 -0.019 m 0.907 -0.202 l 0 -1", 20);

	private static final DisjoncteurDiff dg22 = new DisjoncteurDiff("DG22", "Disjoncteur Diff 40A/300ma 2Phases", 2,
			"m0 0 0-1-1-2-.045.029.096.192.043-.019m-.094 1.798.153-.317.121.314-.274.003m1-2 0-1", 40, 300);
	private static final DisjoncteurDiff df22 = new DisjoncteurDiff("DF22", "Disjoncteur Diff 40A/30ma 2Phases", 2,
			"m0 0 0-1-1-2-.045.029.096.192.043-.019m-.094 1.798.153-.317.121.314-.274.003m1-2 0-1", 40, 30);

	// Accès aux dao
	private DAOFactory factory;

	// Le publisher
	private PublisherEvent publisher;

	// Installation chargée
	@Getter
	private SelectionModel selectionModel;

	// Automate
	private AutomateBloc automate;

	/**
	 * @param factory
	 */
	public Facade(DAOFactory factory) {
		// La fabrique pour accéder aux DAOs
		this.factory = factory;

		// Le Publisher pour publier les évènements
		publisher = new PublisherEvent();

		// maintient de l'élément sélectionné
		selectionModel = new SelectionModel(publisher);

		// automate
		this.automate = new AutomateBloc(factory);

	}

	/**
	 * Charge une installation via son id
	 * Code provisoire
	 * @param id
	 * @return
	 */
	public Optional<Installation> chargeInstallation(int id) {
		Installation currentInstallation = null;
		if (id != 1 && id != 2)
			return Optional.empty();
		currentInstallation = new Installation();
		if (id == 1) {
			currentInstallation.setId(1);
			currentInstallation.setAdresse(new Adresse("21 Av. de la gare", 1457, "Walhain"));
			currentInstallation.setCompteur(
					new Compteur("CG3", "COMPTEUR COMPAGNIE 3Phases", 3, "m 0 0 v 2 h 3 v -2 h -3 v -1 h 3 v 1"));
			var oCable = factory.getCableDAO().getFromID("XVB3G4");
			if (oCable.isPresent())
				currentInstallation.setCable(oCable.get());

			currentInstallation.setDisjoncteurCompagnie(dg22);

			currentInstallation.setDate(LocalDate.of(2024, 11, 22));
			currentInstallation.setInstallateur("SupElec");
			currentInstallation.setTypeLogement(TypeLogement.MAISON);
			// Groupe Ligne
			GroupeLigne groupe;
			try {
				groupe = currentInstallation.addGroupeLigne();

				groupe.setDisjoncteur(dg22);

				// Ligne 1
				Ligne ligne1 = new Ligne();
				factory.getCableDAO().getFromID("XVB3G2").ifPresent(c -> ligne1.setCable(c));
				ligne1.setInterne(true);
				ligne1.setFusible(fu22);
				groupe.addLigne(ligne1);
				// blocs lignes 1
				Bloc bloc1 = ligne1.addBloc();
				bloc1.getAppareils().add(prise);
				bloc1.getAppareils().add(prise);

				Bloc bloc2 = ligne1.addBloc();
				bloc2.getAppareils().add(int1);
				bloc2.getAppareils().add(prise);

				// Ligne 2
				Ligne ligne2 = new Ligne();
				factory.getCableDAO().getFromID("XVB3G2").ifPresent(c -> ligne2.setCable(c));
				ligne2.setInterne(true);
				ligne2.setFusible(fu22);
				groupe.addLigne(ligne2);
				// blocs ligne2
				bloc1 = ligne2.addBloc();
				bloc1.getAppareils().add(prise);
				bloc1.getAppareils().add(prise);

				bloc2 = ligne2.addBloc();
				bloc2.getAppareils().add(int1);
				bloc2.getAppareils().add(prise);

			} catch (InstallationException e) {
				e.printStackTrace();
			}
		} else if (id == 2) {
			currentInstallation.setId(2);
			currentInstallation.setAdresse(new Adresse("Rue Joseph Buedts, 14", 1040, "Etterbeek"));
			currentInstallation.setCompteur(
					new Compteur("CG3", "COMPTEUR COMPAGNIE 3Phases", 3, "m 0 0 v 2 h 3 v -2 h -3 v -1 h 3 v 1"));
			var oCable = factory.getCableDAO().getFromID("XVB3G4");
			if (oCable.isPresent())
				currentInstallation.setCable(oCable.get());

			currentInstallation.setDisjoncteurCompagnie(dg22);

			currentInstallation.setDate(LocalDate.of(2024, 11, 22));
			currentInstallation.setInstallateur("Sidec");
			currentInstallation.setTypeLogement(TypeLogement.MAISON);
			// Groupe Ligne
			GroupeLigne groupe;
			try {
				groupe = currentInstallation.addGroupeLigne();

				groupe.setDisjoncteur(dg22);

				// Ligne 1
				Ligne ligne1 = new Ligne();
				factory.getCableDAO().getFromID("XVB3G2").ifPresent(c -> ligne1.setCable(c));
				ligne1.setInterne(true);
				ligne1.setFusible(fu22);
				groupe.addLigne(ligne1);
				// blocs lignes 1
				Bloc bloc1 = ligne1.addBloc();
				bloc1.getAppareils().add(prise);
				bloc1.getAppareils().add(prise);

				Bloc bloc2 = ligne1.addBloc();
				bloc2.getAppareils().add(prise);
			} catch (InstallationException e) {
				e.printStackTrace();
			}
		}

		selectionModel.setInstallation(currentInstallation);

		// publie l'évènement
		Map<Classe, Evenement> messages = new HashMap<>();
		messages.put(Classe.INSTALLATION, new Evenement(TypeOperation.CHARGEMENT, currentInstallation));
		publisher.submit(new ListMessages(messages));

		return Optional.ofNullable(currentInstallation);
	}

	/**
	 * Création d'un nouvelle installation
	 * Code provisoire
	 * @param installation
	 * @return
	 * @throws InstallationException
	 */
	public Installation createInstallation(Installation installation) throws InstallationException {
		installation.setId(100);
		System.out.println("CREATE: " + installation);

		// TODO

		// publie l'évènement
		Map<Classe, Evenement> messages = new HashMap<>();
		messages.put(Classe.INSTALLATION, new Evenement(TypeOperation.CHARGEMENT, installation));
		publisher.submit(new ListMessages(messages));

		selectionModel.setInstallation(installation);
		return installation;
	}

	// Retourne l'installation chargée
	public Installation getCurrentInstallation() {
		return selectionModel.getCurrentInstallation();
	}

	/**
	 *  CODE provisoire pour faire l'update de l'installation 
	 * @param installation
	 * @throws InstallationException
	 */
	public void updateInstallation(Installation installation) throws InstallationException {
		System.out.println("UPDATE: " + installation);

	}

	/**
	* Affichage basique dans la console 
	* codes provisoires qui seront supprimés ensuite
	*/
	public void afficheInstallation() {
		Installation currentInstallation = selectionModel.getCurrentInstallation();
		if (currentInstallation == null)
			System.out.println("PAS D'INSTALLATION");
		else {
			System.out.println(currentInstallation.toString());
			// GroupeLigne
			List<GroupeLigne> grps = currentInstallation.getGroupeLignes();
			for (GroupeLigne grp1 : grps)
				afficheGroupeLigne(grp1);
		}
	}

	private void afficheGroupeLigne(GroupeLigne grp) {
		System.out.println("-------------GROUPE------------");
		System.out.println(grp);
		for (Ligne ligne : grp.getLignes())
			afficheLigne(ligne);
	}

	private void afficheLigne(Ligne ligne) {
		System.out.println("-------------LIGNE------------");
		System.out.println(ligne);
		for (Bloc bloc : ligne.getBlocs())
			afficheBloc(bloc);
	}

	private void afficheBloc(Bloc bloc) {
		System.out.println(bloc);
		for (Appareil app : bloc.getAppareils())
			System.out.println(app);
	}

	/**
	 * Ajoute un groupeLigne à l'installation	
	 * @return optional de GroupeLigne
	 */
	public Optional<GroupeLigne> addGroupe() {
		GroupeLigne grp = null;

		try {
			grp = selectionModel.getCurrentInstallation().addGroupeLigne();

			// publie l'évènement
			Map<Classe, Evenement> messages = new HashMap<>();
			messages.put(Classe.GROUPELIGNE, new Evenement(TypeOperation.CREATE, grp));
			publisher.submit(new ListMessages(messages));

		} catch (InstallationException e) {
			log.error("Impossible d'ajouter un groupe car l'installation n'existe pas");
		}

		return Optional.ofNullable(grp);
	}

	/**
	 * Ajoute une ligne au groupe
	 * @param groupe
	 */
	public Optional<Ligne> addLigne(GroupeLigne groupe) {
		Ligne ligne = null;
		if (selectionModel.hasInstallation() && groupe != null) {
			ligne = new Ligne();
			groupe.addLigne(ligne);

			// publie l'évènement
			Map<Classe, Evenement> messages = new HashMap<>();
			messages.put(Classe.LIGNE, new Evenement(TypeOperation.CREATE, ligne));
			publisher.submit(new ListMessages(messages));
		}
		return Optional.ofNullable(ligne);
	}

	/**
	 * Ajoute un bloc à une ligne
	 * @param ligne
	 * @return
	 * 
	 */
	public Bloc addBloc(Ligne ligne) throws LigneBlocSizeException {
		assert ligne != null : "La ligne doit exister";
		Bloc bloc = null;
		if (selectionModel.hasInstallation()) {
			bloc = ligne.addBloc();
			// publie l'évènement
			Map<Classe, Evenement> messages = new HashMap<>();
			messages.put(Classe.BLOC, new Evenement(TypeOperation.CREATE, bloc));
			publisher.submit(new ListMessages(messages));
		}
		return bloc;
	}

	/**
	 * Ajoute un bloc et des appareils à partir d'une expression DSL
	 * @param ligne la ligne auquel un bloc doit être rajoutée
	 * @param expr l'expression DSL pour définir des appareils
	 * @returnn le bloc avec ses appareils
	 * @throws UnexpectedCharacterException expression invalide
	 * @throws LigneBlocSizeException on dépasse le nombre de 8 blocs
	 */
	public Bloc addBlocAppareils(Ligne ligne, String expr) throws UnexpectedCharacterException, LigneBlocSizeException {
		assert ligne != null : "La ligne ne peut pas être à null";
		// Crée une liste d'appareils
		var liste = automate.generateListFromExpr(expr);
		// Ajoute un Bloc à la ligne
		Bloc bloc = ligne.addBloc();
		// ajoute les appareils au bloc
		bloc.getAppareils().addAll(liste);
		// publie l'évènement
		Map<Classe, Evenement> messages = new HashMap<>();
		messages.put(Classe.BLOC, new Evenement(TypeOperation.CREATE, bloc));
		publisher.submit(new ListMessages(messages));
		return bloc;
	}

	/**
	 * 
	 * @param obj
	 */
	public void setSelectedItem(IInstallationItems obj) {
		switch (obj) {
		case Installation inst -> selectionModel.setInstallation(inst);
		case GroupeLigne grp -> selectionModel.setCurrentGroupe(grp);
		case Ligne lig -> selectionModel.setCurrentLigne(lig);
		case Bloc bloc -> selectionModel.setCurrentBloc(bloc);
		case Appareil app -> selectionModel.setCurrentAppareil(app);
		default -> selectionModel.setNoSelection();
		}
	}

	public List<CodeApp> getListCodeCable() {
		return factory.getCableDAO().getListeCode();
	};

	public List<CodeApp> getListCodeCompteur() {

		return List.of(new CodeApp("CG1", "Disjoncteur Diff 20A/30ma 2Phases"),
				new CodeApp("CG3", "COMPTEUR COMPAGNIE 3Phases"));
	}

	public List<CodeApp> getListCodeDisjoncteur() {
		// Fake
		return List.of(new CodeApp("DF12", "Disjoncteur Diff 20A/30ma 2Phases"),
				new CodeApp("DF13", "Disjoncteur Diff 20A/30ma 3Phases"),
				new CodeApp("DF22", "Disjoncteur Diff 40A/30ma 2Phases"),
				new CodeApp("DF23", "Disjoncteur Diff 40A/30ma 3Phases"),
				new CodeApp("DG12", "Disjoncteur Diff 20A/300ma 2Phases"),
				new CodeApp("DG13", "Disjoncteur Diff 20A/300ma 3Phases"),
				new CodeApp("DG22", "Disjoncteur Diff 40A/300ma 2Phases"),
				new CodeApp("DG23", "Disjoncteur Diff 40A/300ma 3Phases"),
				new CodeApp("FU02", "Fusible automatique 6A/2Phases"),
				new CodeApp("FU12", "Fusible automatique 16A 2Phases"),
				new CodeApp("FU22", "Fusible automatique 20A 2Phases"),
				new CodeApp("FU32", "Fusible automatique 32 2Phases"));
	}

	public void setDisjoncteur(String code) {
		// Fake fixe le dg22 ou df22
		selectionModel.getCurrentInstallation().setDisjoncteurCompagnie(dg22);
		// Informe que l'installation à changée
	}

	public void setCompteur(String code) {

	}

	public void setCable(String code) {
		this.factory.getCableDAO().getFromID(code).ifPresent((c) -> {
			Classe classe = switch (selectionModel.getSelectedItem()) {
			case Installation inst -> {
				inst.setCable(c);
				yield Classe.INSTALLATION;
			}
			case Ligne ligne -> {
				ligne.setCable(c);
				yield Classe.LIGNE;
			}
			default -> throw new IllegalArgumentException("Unexpected Elem ");
			};
			// publie l'évènement
			Map<Classe, Evenement> messages = new HashMap<>();
			messages.put(classe, new Evenement(TypeOperation.UPDATE, selectionModel.getSelectedItem()));
			publisher.submit(new ListMessages(messages));
		});

	}

	public void saveInstallation() throws Exception {
		var inst = selectionModel.getCurrentInstallation();
		if (inst.getId() != null) {
			// factory.getInstallationDAO().update(inst);
		}
	}

	/**
	 * Supprime la ligne si possible
	 * @param ligne
	 * @throws Exception
	 */
	public void deleteGroupeLigne(GroupeLigne groupe) throws Exception {
		assert groupe != null : "Le groupe ne peut pas être null";
		var installation = selectionModel.getCurrentInstallation();
		installation.removeGroupeLigne(groupe);

		// supprime le groupe de la base de données
		// TODO Activer la ligne suivante lorsque DAO fait
		// factory.getGroupeLigneDAO().delete(groupe);
		// publie l'évènement
		Map<Classe, Evenement> messages = new HashMap<>();
		messages.put(Classe.GROUPELIGNE, new Evenement(TypeOperation.DELETE, groupe));
		publisher.submit(new ListMessages(messages));

	}

	/**
	 * Supprime la ligne si possible
	 * @param ligne
	 * @throws Exception
	 */
	public void deleteLigne(Ligne ligne) throws Exception {
		assert ligne != null : "La ligne ne peut pas être null";
		// Récupère l'identifiant de son groupeLigne
		Integer idGroupe = ligne.getNum() / 100;
		// recherche le groupe:
		GroupeLigne grp = selectionModel.getCurrentInstallation().getGroupeLignes().stream()
				.filter(g -> g.getNum().equals(idGroupe)).findFirst().orElseThrow();
		grp.removeLigne(ligne.getNum());
		// supprime la ligne de la base de données
		// TODO A décocher lorsque le DAO Existe
		// factory.getLigneDAO().delete(ligne);
		// publie l'évènement
		Map<Classe, Evenement> messages = new HashMap<>();
		messages.put(Classe.LIGNE, new Evenement(TypeOperation.DELETE, ligne));
		publisher.submit(new ListMessages(messages));

	}

	/**
	 * Supprime le bloc
	 * @param bloc
	 * @throws Exception 
	 */
	public void deleteBloc(Bloc bloc) throws Exception {
		assert bloc != null : "Le bloc ne peut pas être null";
		// Récupère l'identifiant de sa ligne et de son groupe
		Integer idLigne = bloc.getId() / 10;
		Integer idGroupe = idLigne / 100;
		// recherche le groupe de la ligne:
		GroupeLigne grp = selectionModel.getCurrentInstallation().getGroupeLignes().stream()
				.filter(g -> g.getNum().equals(idGroupe)).findFirst().orElseThrow();
		// recherche la ligne du groupe
		Ligne ligne = grp.getLignes().stream().filter(l -> l.getNum().equals(idLigne)).findFirst().orElseThrow();
		// supprime le bloc de la ligne
		ligne.removeBloc(bloc);
		// supprime de la base de données
//TODO A activer lorsque le DAO est fait
//factory.getBlocDAO().delete(bloc);

		// publie l'évènement
		Map<Classe, Evenement> messages = new HashMap<>();
		messages.put(Classe.BLOC, new Evenement(TypeOperation.DELETE, bloc));
		publisher.submit(new ListMessages(messages));
	}

	/**
	 * Permet de s'abonner aux évènements
	 * @param treeBuilder
	 */
	public void addObserver(Subscriber<ListMessages> abonne) {
		publisher.addObserver(abonne);
	}

	/**
	 * Permettra de choisir une installation
	 * @return
	 */
	public List<RInstallation> getListeInstallations() {
		// Code fake à retirer lorsque le dao existe
		List<RInstallation> liste = new ArrayList<RInstallation>();
		for (int i = 1; i < 10; i++)
			liste.add(new RInstallation(i, LocalDate.now(), "Rue " + i, 1040, "Etterbeek"));
		return liste;
		// TODO code a activer
		// return factory.getInstallationDAO().getListeRecord();
	}

}
